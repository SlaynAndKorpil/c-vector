# A vector in C

## Description
This is the implementation of a dynamically resizable, contiguous data structure in C.  
The interface is similar to [std::vec::Vec](https://doc.rust-lang.org/std/vec/struct.Vec.html) in the Rust standard library.

This repo will contain three different implementations: simple, complex, and generic.
Currently only the simple version is implemented.

See the documentation in the respective `vector.h` files for more details.

## Simple vector

The type contained in this vector is simply defined with a `#define` macro, so the implementation needs to be copied for every different type to put in a vector.

It also doesn't have any way to automatically clean up resources (simliar to Rust's [Drop](https://doc.rust-lang.org/core/ops/trait.Drop.html)) associated with elements in the vector when these elements are deleted. This might cause leaks of resources like memory or file handles.  
For example, when using the `vec_clear` function on a vector of opened file handles, these handles won't get closed.

## Complex vector

Same as the simple vector but it calls a function to free and clean up its elements when they get deleted.

## Generic vector

A (runtime) generic vector that can hold elements of any type (but still only one type per vector).
Therefore there is no need to copy this vector for every different element type. Instead the vector struct holds infomation about the length of its elements and how to clean them when deleting them.
