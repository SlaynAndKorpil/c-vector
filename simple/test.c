#include <stdio.h>
#include <stdlib.h>

#include "vector.h"

#define TEST 1

#if TEST == 0
static void print_int(const int *const value) {
	printf("%d \t", *value);
}

static void inc(int *const value) {
	*value += 1;
}

static bool is_even(const int *const value) {
	return *value % 2 == 0;
}

static int int_cmp(const int *const x, const int *const y) {
	return *x - *y;
}

int main(void) {
	vector vec = vec_new();
	vector vec2 = vec_new();

	if (vec_reserve(&vec, 10) != VECTOR_SUCCESS) {
		fprintf(stderr, "Failed to reserve!\n");
		return 1;
	}

	for (int i = 129; i <= 299; i += 1)
		if (vec_push(&vec2, i) != VECTOR_SUCCESS) {
			fprintf(stderr, "Failed to realloc vector!\n");
			return 1;
		}

	for (int i = 1; i <= 128; i += 1)
		if (vec_push(&vec, i) != VECTOR_SUCCESS) {
			fprintf(stderr, "Failed to realloc vector!\n");
			return 1;
		}

	if (vec_insert(&vec, vec.len - 2, 1337) != VECTOR_SUCCESS) {
		fprintf(stderr, "Failed to insert!\n");
		return 1;
	}
	if (vec_append(&vec, &vec2) != VECTOR_SUCCESS) {
		fprintf(stderr, "Failed to append!\n");
		return 1;
	}
	if (vec_set(&vec, 29, -1337) != VECTOR_SUCCESS) {
		fprintf(stderr, "Failed to set!\n");
		return 1;
	}
	if (vec_insert(&vec, 1, -1000) != VECTOR_SUCCESS) {
		fprintf(stderr, "Failed to insert!\n");
		return 1;
	}

	int value = 0;
	if (vec_remove(&vec, 100, &value) != VECTOR_SUCCESS) {
		fprintf(stderr, "Failed to swap remove!\n");
		return 1;
	}
	if (vec_remove(&vec, vec.len - 1, NULL) != VECTOR_SUCCESS) {
		fprintf(stderr, "Failed to remove!\n");
		return 1;
	}
	if (vec_remove(&vec, 0, NULL) != VECTOR_SUCCESS) {
		fprintf(stderr, "Failed to remove!\n");
		return 1;
	}
	if (vec_swap_remove(&vec, 1, NULL) != VECTOR_SUCCESS) {
		fprintf(stderr, "Failed to swap remove!\n");
		return 1;
	}
	if (vec_swap(&vec, 5, 6) != VECTOR_SUCCESS) {
		fprintf(stderr, "Failed to swap!\n");
	}
	vec_map(&vec, inc);

	if (vec_shrink_to_fit(&vec) != VECTOR_SUCCESS) {
		fprintf(stderr, "Failed to shrink_to_fit!\n");
		return 1;
	}

	printf("vec len: %zu\n", vec.len);
	printf("vec cap: %zu\n", vec.cap);

	if (vec_resize(&vec, 399, 1337) != VECTOR_SUCCESS) {
		fprintf(stderr, "Failed to shrink_to_fit!\n");
		return 1;
	}

	printf("\nvec:\n");
	vec_foreach(&vec, print_int);
	printf("\nvec len: %zu\n", vec.len);
	printf("vec cap: %zu\n", vec.cap);

	vec_retain(&vec2, is_even);
	//vec_split_off(&vec, &vec2, vec.len - 50);

	vec_reverse(&vec);

	printf("\nvec:\n");
	vec_foreach(&vec, print_int);
	printf("\nvec len: %zu\n", vec.len);
	printf("vec cap: %zu\n", vec.cap);

	printf("\nvec2:\n");
	vec_foreach(&vec2, print_int);
	printf("\nvec2 len: %zu\n", vec2.len);
	printf("vec2 cap: %zu\n", vec2.cap);

	if (vec_is_sorted(&vec2, int_cmp)) {
		size_t found_idx;
		if (vec_binary_search(&vec2, 301, &found_idx, int_cmp) != VECTOR_SUCCESS)
			printf("Not found. Insert at %zu!\n", found_idx);
		else
			printf("Found at %zu!\n", found_idx);
	} else {
		printf("vec is not sorted!\n");
	}

	vec_rotate_left(&vec, 10);
	vec_sort(&vec, int_cmp);

	printf("\nvec:\n");
	vec_foreach(&vec, print_int);
	printf("\nvec len: %zu\n", vec.len);
	printf("vec cap: %zu\n", vec.cap);

	vec_free(&vec);
	vec_free(&vec2);
	return 0;
}
#endif

#if TEST == 1
#include <string.h>

static void print_char(const char *const c) {
	putchar(*c);
}

static bool is_lowercase(const char *const c) {
	return 'a' <= *c && *c <= 'z';
}

static void to_uppercase(char *const c) {
	if (is_lowercase(c))
		*c ^= 0x20;
}

int main(void) {
	vector string1 = vec_new();
	vector string2 = vec_new();
	const char *bye = "Bye, world!\n";

	vec_push(&string1, 'e');
	vec_push(&string1, 'l');
	vec_push(&string1, 'l');
	vec_push(&string1, 'o');
	vec_push(&string1, ',');
	vec_push(&string1, ' ');
	vec_push_rev(&string1, 'H');

	vec_push(&string2, 'W');
	vec_push(&string2, 'o');
	vec_push(&string2, 'r');
	vec_push(&string2, 'l');
	vec_push(&string2, 'd');
	vec_push(&string2, '!');
	vec_push(&string2, '\n');

	vec_append(&string1, &string2);
	vec_append_arr(&string1, bye, strlen(bye));

	size_t idx = 0;
	while (vec_find_next_arr(&string1, bye, strlen(bye), idx, &idx) == VECTOR_SUCCESS) {
		printf("Found \"%s\" at %zu!\n", bye, idx);
		++idx;
	}

	if (vec_strip_prefix(&string1, "Hello, ", strlen("Hello, ")))
		printf("Stripped successfully!\n");
	else
		printf("Stripped unsuccessfully!\n");

	if (vec_strip_suffix(&string1, "Bye, world!\n", strlen("Bye, world!\n")))
		printf("Stripped successfully!\n");
	else
		printf("Stripped unsuccessfully!\n");

	printf("\nstring1:\n");
	vec_foreach(&string1, print_char);

	if (vec_ends_with(&string1, string1.ptr, string1.len))
		printf("\nString ends with.\n");
	else
		printf("\nString does not end with.\n");

	vec_retain(&string1, is_lowercase);
	vec_map(&string1, to_uppercase);

	printf("\nstring1:\n");
	vec_foreach(&string1, print_char);
	printf("\nstring2:\n");
	vec_foreach(&string2, print_char);

	idx = string1.len - 1;
	while (vec_find_next_rev(&string1, 'E', idx, &idx) == VECTOR_SUCCESS) {
		printf("found %c at %zu!\n", string1.ptr[idx], idx);
		--idx;
	}

	vec_free(&string1);
	vec_free(&string2);
}
#endif

#if TEST == 2
#define VEC_SIZE 1024LLU * 1024LLU * 1024LLU * 3LLU
static void print_int(const int *const value) {
	printf("%d, ", *value);
}

static int int_cmp(const int *const x, const int *const y) {
	return *x - *y;
}
static int int_cmp_rev(const int *const x, const int *const y) {
	return *y - *x;
}

int main(void) {
	vector vec = vec_new();
	if (vec_reserve(&vec, VEC_SIZE) != VECTOR_SUCCESS) {
		fprintf(stdout, "Failed to reserve enough space!\n");
		return 1;
	}

	srand(1000);
	for (size_t i = 0; i < VEC_SIZE; ++i)
		vec_push(&vec, rand());

	fprintf(stdout, "\n\nFinished generating the vector.\n");
	fprintf(stdout, "vec cap: %zu, len: %zu\n", vec.cap, vec.len);
	fprintf(stdout, "vec byte size: %zu\n", vec.len * sizeof(TYPE));

	vec_sort(&vec, int_cmp);
	fprintf(stdout, "\n\nFinished sorting the vector.\n");
	vec_reverse(&vec);
	vec_foreach(&vec, print_int);
	fprintf(stdout, "\n\nFinished reversing the vector.\n");

	if (vec_is_sorted(&vec, int_cmp_rev))
		fprintf(stdout, "vec is sorted!\n");
	else
		fprintf(stdout, "vec is not sorted!\n");

	vec_free(&vec);
	return 0;
}
#endif
