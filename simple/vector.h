#pragma once

#include <stdbool.h>

/*
 * =======================================================================================
 *     Start of the configuration
 * Uncomment and change the lines below to fit your needs.
 * =======================================================================================
 */

/*
 * The type stored in the vector.
 *
 * sizeof(TYPE) MUST NOT be 0, this will lead to many erroneously reported allocation
 * failures.
 *
 * NOTE:
 *  It should be possible to make the vector capable of holding values with size 0 by
 *  checking for sizeof(TYPE) before every attempt to allocate memory and having vec->ptr
 *  always be NULL.
 */
#define TYPE char

/*
 * Uncomment the next line to make all functions that operate on a vector print an error
 * message and abort whenever an allocation fails instead of returning an error code.
 *
 * Since printing also might allocate some memory, this might lead to unwanted behavior.
 */
/*
#define VEC_ABORT_ON_ALLOC_FAIL
*/

/*
 * Defines how the size of the space allocated for the vector should grow when
 * reallocating.
 * The new size calculated by this macro is expected to always be greater than the current
 * size `cur_size`.
 */
#define VEC_CAPACITY_GROW(cur_size) (cur_size * 2)

/*
 * Defines the minimum starting capacity for the first allocation of a vector.
 *
 * Choose a small number for vectors that are expected to contain only a small amount of
 * elements and grow slowly or a higher number to reduce the amount of reallocations.
 */
#define VEC_MIN_CAPACITY 4

/*
 * Size in bytes of a static buffer that is used by some functions.
 * Since that buffer is only used for performance reasons, there is no necessary minimum
 * size.
 */
#define BUFFER_SIZE 1024 * 1024


/*
 * =======================================================================================
 *     End of the configuration
 * Everything below here is the actual vector implementation.
 * =======================================================================================
 */

/*
 * Results that get returned by functions operating on vectors.
 * See the individual function documentations for specific use.
 */
typedef enum {
	/* Success! */
	VECTOR_SUCCESS,
	/*
	 * Indicates that a given index is not a valid index into the vector or there is some
	 * other problem regarding the length of the vector.
	 */
	VECTOR_IDX_FAIL,
	/* Might get returned by VEC_ALLOC_FAIL_REACTION on an allocation failure */
	VECTOR_ALLOC_FAIL,
} vector_result;

/*
 * This is a contiguous growable and shrinkable array.
 *  `ptr`: A pointer to the first element of the vector, NULL when `cap` is 0.
 *  `len`: The amount of elements in this vector. It is guaranteed to always be smaller or
 *         equal to `cap`.
 *  `cap`: The maximum amount of elements this vector can hold with the currently
 *         allocated space. 0 when `ptr` is NULL.
 *
 * It is not recommended to manually change any of these fields. Instead use the functions
 * below to interact with the vector.
 * If you have to directly change any of these values, make sure to maintain the
 * constraints mentioned above.
 *
 * Since this vector only does free() its own allocation it is not suitable to hold
 * structs which themself have to clean up resources when getting deleted (e.g. opened
 * files, network sockets, memory allocations) since these resources will get leaked.
 * Therefore it is not recommended to have a multi-dimensional vector by storing other
 * vectors inside a vector.
 *
 * For vector implementations that can handle more complex structures, see the complex and
 * the generic version. (TODO: currently unimplemented)
 *
 * All functions below with return type vector_result return:
 *  - VECTOR_SUCCESS on success
 *  - VECTOR_IDX_FAIL when an index is out of bounds
 *  - VECTOR_ALLOC_FAIL on memory allocation (malloc, realloc, calloc) failure by default,
 *                      this behavior can be changed by
 *                      defining VEC_ABORT_ON_ALLOC_FAIL (see configuration above)
 *
 * It is guaranteed that even when a function fails with one of the failure codes
 * (VECTOR_IDX_FAIL or VECTOR_ALLOC_FAIL), the vector is still in the same state as
 * before the function call. This means that it can still be used safely.
 *
 * NOTE:
 *  All functions below that work with a vector* pointer assume that the vector is
 *  already initialized via vec_new()! (i.e. a valid pointer to a vector struct)
 */
typedef struct {
	TYPE *ptr;
	size_t len;
	size_t cap;
} vector;

/*
 * Creates a new vector and returns it.
 * Allocates no memory for the vector, so len and cap are 0 and ptr is NULL.
 */
inline vector vec_new(void) __attribute__((always_inline));

/*
 * C is weird. Apparently you have to place the implementation in the same file as the
 * prototype when defining an inline function. Also it's not possible to remove the
 * prototype.
 */
inline vector vec_new(void) {
	vector vec = {
		.ptr = NULL,
		.len = 0,
		.cap = 0,
	};
	return vec;
}

/*
 * Pushes a new element at the end of the vector.
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_ALLOC_FAIL when failing to allocate enough
 * space for the new element.
 */
vector_result vec_push(vector *vec, TYPE value);

/*
 * Pushes a new element at the start of the vector.
 * While vec_push() is fast (O(1)) because it only has to append the new value,
 * vec_push_rev() always has to move all elements of the vector (which is O(n)).
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_ALLOC_FAIL when failing to allocate enough
 * space for the new element.
 */
vector_result vec_push_rev(vector *vec, TYPE value);

/*
 * Pops the last element off of the end of the vector into the `into` parameter.
 * The popped element gets discarded when `into` is NULL.
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_IDX_FAIL when the vector is empty.
 */
vector_result vec_pop(vector *vec, TYPE *into);

/*
 * Pops the first element off of the start of the vector into the `into` parameter.
 * The popped element gets discarded when `into` is NULL.
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_IDX_FAIL when the vector is empty.
 */
vector_result vec_pop_rev(vector *vec, TYPE *into);

/*
 * Sets the value at a given index.
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_IDX_FAIL when idx is not a valid index of
 * the vector.
 */
vector_result vec_set(vector *vec, size_t idx, TYPE value);

/*
 * Copies the value at index `idx` of the vector into `into`.
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_IDX_FAIL when idx is not a valid index of
 * the vector.
 */
vector_result vec_get(const vector *vec, size_t idx, TYPE *into);

/*
 * Inserts a new element at the given index.
 * This moves all elements of the vector starting at `idx` to the right.
 * When `idx` == vec->len, vec_insert() appends the `value` to the end of the vector.
 *
 * Example:
 *  +----+----+----+----+
 *  | 14 |  3 | 69 | -1 |
 *  +----+----+----+----+
 *           ||
 *      vec_insert(&vec, 2, 13);
 *           ||
 *           \/
 *  +----+----+----+----+----+
 *  | 14 |  3 | 13 | 69 | -1 |
 *  +----+----+----+----+----+
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_IDX_FAIL when `idx` is not a valid index of
 * the vector or VECTOR_ALLOC_FAIL when malloc fails.
 */
vector_result vec_insert(vector *vec, size_t idx, TYPE value);

/*
 * Removes an element from the vector at a given index and moves all elements of the
 * vector starting at `idx`+1 to the left to ensure the vector remains contiguous.
 * This keeps the order of the values in the vector.
 *
 * The removed element gets discarded when `into` is NULL.
 *
 * Example:
 *  +----+----+----+----+----+
 *  | 14 |  3 | 13 | 69 | -1 |
 *  +----+----+----+----+----+
 *           ||
 *      vec_remove(&vec, 2, &result);
 *           ||
 *           \/
 *  +----+----+----+----+
 *  | 14 |  3 | 69 | -1 | => variable "result" is now 13
 *  +----+----+----+----+
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_IDX_FAIL when idx is not a valid index of
 * the vector.
 */
vector_result vec_remove(vector *vec, size_t idx, TYPE *into);

/*
 * Removes an element from the vector at `idx`, then puts the last element of the
 * vector at its place.
 * This changes the order of the elements but is faster than vec_remove().
 * The removed element gets discarded when `into` is NULL.
 *
 * Example:
 *  +----+----+----+----+----+
 *  | 12 | 15 | -3 | 89 | -8 |
 *  +----+----+----+----+----+
 *           ||
 *  vec_swap_remove(&vec, 1, &result);
 *           ||
 *           \/
 *  +----+----+----+----+
 *  | 12 | -8 | -3 | 89 | => variable "result" is now 15
 *  +----+----+----+----+
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_IDX_FAIL when idx is not a valid index of
 * the vector.
 */
vector_result vec_swap_remove(vector *vec, size_t idx, TYPE *into);

/*
 * Appends all elements of the second vector to the first.
 * The vectors must not be the same.
 *
 * Example:
 * vec1:
 *  +----+----+----+
 *  | 12 | 15 | -3 |
 *  +----+----+----+
 * vec2:
 *  +----+----+----+----+
 *  | 42 |  5 | 47 | 49 |
 *  +----+----+----+----+
 *           ||
 *  vec_append(&vec1, &vec2);
 *           ||
 *           \/
 * vec1:
 *  +----+----+----+----+----+----+----+
 *  | 12 | 15 | -3 | 42 |  5 | 47 | 49 |
 *  +----+----+----+----+----+----+----+
 * vec2:
 *  +----+----+----+----+
 *  | 42 |  5 | 47 | 49 |
 *  +----+----+----+----+
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_ALLOC_FAIL when not enough space could be
 * allocated.
 */
vector_result vec_append(vector *restrict vec, const vector *restrict other);

/*
 * Appends `len` elements from `arr` to `vec`.
 * Basically the same as vec_append, just using an array instead of a vector to copy from.
 */
vector_result vec_append_arr(vector *vec, TYPE const *arr, size_t len);

/*
 * Resizes the vector to `new_len`.
 * When `new_len` > vec->len, the additional space gets filled with copies of `value`.
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_ALLOC_FAIL when failing to allocate more
 * space.
 */
vector_result vec_resize(vector *vec, size_t new_len, TYPE value);

/*
 * Truncates the vector so its length is at most min(`new_len`, vec->len).
 *
 * This will not change the actually allocated space. Use vec_shrink_to for that.
 *
 * The same as vec_resize() when only using `new_len`s that are smaller than the current
 * size of the vector.
 */
void vec_truncate(vector *vec, size_t new_len);

/*
 * Sets the size of the vector to 0.
 *
 * This will not change the actually allocated space. Use vec_shrink_to for that.
 *
 * The same as vec_truncate(&vec, 0).
 */
void vec_clear(vector *vec);

/*
 * Shrinks the space allocated for the vector to max(vec->len, min_capacity).
 *
 * Does nothing when current capacity is already smaller than `min_capacity`.
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_ALLOC_FAIL when failing to allocate a smaller
 * space.
 */
vector_result vec_shrink_to(vector *vec, size_t min_capacity);

/*
 * Shrinks the space allocated for the vector to be the same as the length of the vector.
 *
 * The same as vec_shrink_to(&vec, vec.len).
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_ALLOC_FAIL when failing to allocate a smaller
 * space.
 */
vector_result vec_shrink_to_fit(vector *vec);

/*
 * Adjusts the capacity to fit at least `additional` more elements.
 * This may (and often will) reserve space for more then `additional` elements for
 * performance reasons. See vec_reserve_exact for a version that always allocates exactly
 * the wanted space.
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_ALLOC_FAIL when failing to allocate more
 * space.
 */
vector_result vec_reserve(vector *vec, size_t additional);

/*
 * Adjusts the capacity so that the vector can fit exactly `additional` elements more.
 *
 * Returns VECTOR_SUCCESS on success, VECTOR_ALLOC_FAIL when failing to allocate more
 * space.
 */
vector_result vec_reserve_exact(vector *vec, size_t additional);

/*
 * Returns 1 if the vector contains no elements.
 */
bool vec_is_empty(const vector *vec);

/*
 * Retains only the elements of the vector that are specified by the predicate.
 * The elements keep their order.
 */
void vec_retain(vector *vec, bool (*pred)(TYPE const *));

/*
 * Retains only the elements of the vector that are specified by the predicate.
 */
void vec_retain_unordered(vector *vec, bool (*pred)(TYPE const *));

/*
 * Calls the function `func` with every element in the vector.
 */
void vec_foreach(const vector *vec, void (*func)(TYPE const *));

/*
 * Calls the function `func` with every element in the vector that can replace this
 * element in the vector.
 */
void vec_map(vector *vec, void (*func)(TYPE *));

/*
 * Splits the vector at index `at` and appends all elements starting at `at` in `into`.
 * `into` has to have space for at least (vec->len - at) elements.
 * `into` MUST NOT be a pointer to the allocation of `vec`.
 */
vector_result vec_split_off(vector *vec, vector *into, size_t at);

/*
 * `into` must be a valid pointer to space for at least `vec->len - at` elements.
 */
vector_result vec_split_off_arr(vector *vec, TYPE into[], size_t at);

/*
 * On success: `found_idx` is the index of the found element.
 * Otherwise:  `found_idx` is an index where the element could be inserted with vec_insert
 *             while maintaining the sorting order.
 *
 * Assumes a sorted vector.
 */
vector_result vec_binary_search(
		const vector *vec,
		TYPE x,
		size_t *found_idx,
		int (*ord)(TYPE const *, TYPE const *));

/*
 * Returns true when x is an element of vec, otherwise false.
 */
bool vec_contains(const vector *vec, TYPE x);

/*
 * Returns 1 when the first `len` elements of `vec` are identical to `arr`.
 */
bool vec_starts_with(const vector *vec, const TYPE arr[], size_t len);

/*
 * Tests if the last `len` elements of `vec` are identical to `arr`.
 */
bool vec_ends_with(const vector *vec, TYPE const arr[], size_t len);

/*
 * Tests if the contents of both vectors are equal.
 */
bool vec_equals(const vector *vec, const vector *other);

/*
 * Tests if the contents of the vectors and the array with length `len` are equal.
 */
bool vec_equals_arr(const vector *vec, TYPE const arr[], size_t len);

/*
 * Compares two vectors with a order function.
 * Returns 0 if they are equal or the result of the `ord` function if it returns something
 * else than 0 when comparing elements of the vectors.
 */
int vec_cmp(const vector *vec,
            const vector *other,
            int (*ord)(TYPE const*, TYPE const *));

/*
 * Compares a vector with an array of length `len` using an order function.
 * Returns 0 if they are equal or the result of the `ord` function if it returns something
 * else than 0 when comparing elements.
 */
int vec_cmp_arr(const vector *vec,
                TYPE const arr[],
                size_t len,
                int (*ord)(TYPE const *, TYPE const *));

/*
 * Returns true when all elements of `vec` are sorted in ascending order according to
 * `ord`.
 */
bool vec_is_sorted(const vector *vec, int (*ord)(TYPE const *, TYPE const *));

/*
 * Reverses the vector in place.
 */
void vec_reverse(vector *vec);

/*
 * Rotates the vector right `n` times.
 */
void vec_rotate_right(vector *vec, size_t n);

/*
 * Rotates the vector left `n` times.
 */
void vec_rotate_left(vector *vec, size_t n);

/*
 * On success: Sets `idx` to index of next occurence of `x` starting at index `from`.
 *
 * Example:
 *
 * const char *text = "Hello, world!\n";
 * vector string = vec_new();
 * vec_extend_from_array(&string, text, strlen(text));
 *
 * size_t idx = 0;
 * while (vec_find_next(&string, 'l', idx, &idx) == VECTOR_SUCCESS) {
 *     printf("found %c at %zu!\n", string.ptr[idx], idx);
 *     ++idx;
 * }
 */
vector_result vec_find_next(const vector *vec, TYPE x, size_t from, size_t *idx);

vector_result vec_find_next_arr(const vector *vec,
                                  TYPE const arr[],
                                  size_t len,
                                  size_t from,
                                  size_t *idx);

vector_result vec_find_next_vec(const vector *vec,
                                  const vector *search,
                                  size_t from,
                                  size_t *idx);

vector_result vec_find_next_rev(const vector *vec, TYPE x, size_t from, size_t *idx);

vector_result vec_find_next_arr_rev(const vector *vec,
                                      TYPE const arr[],
                                      size_t len,
                                      size_t from,
                                      size_t *idx);

vector_result vec_find_next_vec_rev(const vector *vec,
                                      const vector *search,
                                      size_t from,
                                      size_t *idx);

/*
 * Returns a pointer to the element of `vec` at `idx`.
 * NULL when `idx` is not a valid index of `vec`.
 *
 * This pointer might get invalidated when calling any function that changes the vector in
 * any way. Do not continue to use the pointer after that.
 */
TYPE *vec_ptr_to(const vector *vec, size_t idx);

/*
 * Copies `len` elements of the vector from index `start` into `into`.
 * `into` MUST NOT be a pointer into `vec`.
 */
vector_result vec_copy_to(const vector *vec, size_t start, size_t len, TYPE into[]);

/*
 * Sort the vector.
 */
void vec_sort(vector *vec, int (*ord)(TYPE const *, TYPE const *));

/*
 * Removes a prefix from `vec`.
 */
bool vec_strip_prefix(vector *vec, TYPE const prefix[], size_t len);

/*
 * Removes a suffix from `vec`.
 */
bool vec_strip_suffix(vector *vec, TYPE const suffix[], size_t len);

/*
 * Swaps the elements with index `a` and `b` in the vector.
 */
vector_result vec_swap(vector *vec, size_t a, size_t b);

/*
 * Frees the allocated memory.
 *
 * Does nothing when no memory is allocated for this vector.
 *
 * NOTE:
 *  Since this vector implementation has no way of cleaning up/freeing its elements
 *  beyond just freeing the space where they are stored, any memory allocations, opened
 *  sockets, etc. of the elements of the vector are leaked.
 *  Use the generic or complex vector implementation to for such elements.
 */
void vec_free(vector *vec);
