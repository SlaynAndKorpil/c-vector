#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "vector.h"

/*
 * The macro VEC_ALLOC_FAIL_REACTION is used whenever an attempt to resize the vector via
 * calls to malloc/calloc/etc failed.
 * The parameter `function_name` is the name of the function in which the failed
 * allocation call happened.
 *
 * This always exits from the function where it is called (either via return or abort).
 *
 * Define VEC_ABORT_ON_ALLOC_FAIL to print an error message and abort on alloc fail.
 */
#ifdef VEC_ABORT_ON_ALLOC_FAIL

/*
 * Abort after printing an error message.
 * NOTE: printf also might fail when OOM
 */
#include <stdio.h>
#include <stdlib.h>
#define VEC_ALLOC_FAIL_REACTION(function_name) {	                        \
	fprintf(stderr, "Failed to allocate in function %s!\n", function_name); \
	abort();                                                                \
}

#else

/*
 * Return the error code defined in vector_result which can then be used by the caller
 * to either work around the allocation failure or gracefully exit.
 */
#define VEC_ALLOC_FAIL_REACTION(fn) return VECTOR_ALLOC_FAIL;

#endif

/*
 * A buffer that can be used by any of the functions below.
 * There is no guarantee that the contents of it are persistant between function
 * calls.
 */
static unsigned char BUFFER[BUFFER_SIZE];

/*
 * Works just like realloc, but with the additional check that nmemb * size does not
 * overflow (similar to calloc).
 *
 * nmemb * size MUST NOT be 0.
 *
 * See Linux or OpenBSD MALLOC(3) manpage.
 */
static void *reallocarray(void *const optr, const size_t nmemb, const size_t size) {
	unsigned long length;
	/*
	 * Using compiler built-in here for better code gen.
	 *
	 * If there are any compatibility problems arising from this, try the OpenBSD
	 * implementation.
	 */
	if (__builtin_umull_overflow(nmemb, size, &length)) {
		errno = ENOMEM;
		return NULL;
	}
	return realloc(optr, length);
}

vector_result vec_push(vector *const vec, TYPE const element) {
	if (vec->len >= vec->cap) { /* too little space, realloc */
		vector_result res = vec_reserve(vec, 1);
		if (res != VECTOR_SUCCESS) {
			return res;
		}
	}

	*(vec->ptr + vec->len) = element;
	++vec->len;

	return VECTOR_SUCCESS;
}

vector_result vec_push_rev(vector *const vec, TYPE const element) {
	return vec_insert(vec, 0, element);
}

vector_result vec_pop(vector *const vec, TYPE *const into) {
	if (vec->len == 0) /* vec is empty */
		return VECTOR_IDX_FAIL;

	--vec->len;
	if (into)
		*into = *(vec->ptr + vec->len);

	return VECTOR_SUCCESS;
}

vector_result vec_pop_rev(vector *const vec, TYPE *const into) {
	return vec_remove(vec, 0, into);
}

vector_result vec_set(vector *const vec, const size_t idx, TYPE const value) {
	if (idx >= vec->len)
		return VECTOR_IDX_FAIL;

	*(vec->ptr + idx) = value;
	return VECTOR_SUCCESS;
}

vector_result vec_get(const vector *const vec, const size_t idx, TYPE *const into) {
	if (idx >= vec->len)
		return VECTOR_IDX_FAIL;

	*into = *(vec->ptr + idx);
	return VECTOR_SUCCESS;
}

vector_result vec_insert(vector *const vec, const size_t idx, TYPE const element) {
	if (idx > vec->len) /* index out of bounds */
		return VECTOR_IDX_FAIL;

	if (vec->len == vec->cap) { /* too litle space, realloc */
		vector_result res = vec_reserve(vec, 1);
		if (res != VECTOR_SUCCESS) {
			return res;
		}
	}

	/* move all elements from idx on to the right by one */
	memmove(vec->ptr + idx + 1,
			vec->ptr + idx,
			(vec->len - idx) * sizeof(TYPE));
	*(vec->ptr + idx) = element;

	++vec->len;

	return VECTOR_SUCCESS;
}

vector_result vec_remove(vector *const vec, const size_t idx, TYPE *const into) {
	if (idx >= vec->len)
		return VECTOR_IDX_FAIL;

	if (into)
		*into = *(vec->ptr + idx);

	--vec->len;
	memmove(vec->ptr + idx,
	        vec->ptr + idx + 1,
	        (vec->len - idx) * sizeof(TYPE));

	return VECTOR_SUCCESS;
}

vector_result vec_swap_remove(vector *const vec, const size_t idx, TYPE *const into) {
	if (idx >= vec->len)
		return VECTOR_IDX_FAIL;

	--vec->len;
	/* removing the last element */
	if (into)
		*into = *(vec->ptr + idx);

	if (idx != vec->len) {
		/* put last vector element in its place */
		*(vec->ptr + idx) = *(vec->ptr + vec->len);
	}

	return VECTOR_SUCCESS;
}

vector_result vec_append(
		vector *restrict const vec,
		const vector *restrict const other
	) {
	// The vectors must be different.
	assert(vec != other);

	return vec_append_arr(vec, other->ptr, other->len);
}

vector_result vec_append_arr(
		vector *const vec,
		TYPE const *const arr,
		const size_t len
	) {
	// The array must point to a seperate memory location.
	assert(vec->ptr != arr);

	const vector_result reserve_result = vec_reserve(vec, len);
	if (reserve_result != VECTOR_SUCCESS)
		return reserve_result;

	memcpy(vec->ptr + vec->len, arr, len * sizeof(TYPE));
	vec->len += len;

	return VECTOR_SUCCESS;
}

vector_result vec_resize(vector *const vec, const size_t new_len, TYPE const value) {
	if (new_len > vec->cap) {
		const vector_result reserve_result = vec_reserve(vec, new_len - vec->len);
		if (reserve_result != VECTOR_SUCCESS)
			return reserve_result; /* propagate failure */
	}

	for (size_t i = vec->len; i < new_len; ++i)
		*(vec->ptr + i) = value;

	vec->len = new_len;

	return VECTOR_SUCCESS;
}

void vec_truncate(vector *const vec, const size_t new_len) {
	if (new_len < vec->len)
		vec->len = new_len;
}

void vec_clear(vector *const vec) {
	vec->len = 0;
}

vector_result vec_shrink_to(vector *const vec, const size_t min_capacity) {
	if (min_capacity >= vec->cap) /* do nothing when capacity is already small enough */
		return VECTOR_SUCCESS;

	/*           new_cap = max(vec->len, min_capacity) */
	const size_t new_cap = vec->len > min_capacity ? vec->len : min_capacity;

	if (new_cap == 0) { /* we can't calloc(0, _) since that always returns NULL */
		free(vec->ptr); /* vec->cap > 0, or we would have exited at start of function */
		vec->ptr = NULL;
		vec->cap = 0;
	} else {
		TYPE *new_ptr = reallocarray(vec->ptr, new_cap, sizeof(TYPE));
		if (new_ptr == NULL)
			VEC_ALLOC_FAIL_REACTION("vec_shrink_to")

		vec->ptr = new_ptr;
		vec->cap = new_cap;
	}

	return VECTOR_SUCCESS;
}

vector_result vec_shrink_to_fit(vector *const vec) {
	return vec_shrink_to(vec, vec->len);
}

/*
 * Calculates the new increased capacity that is at least 'target_cap' by using a growth
 * strategy honoring 'VEC_MIN_CAPACITY' and 'VEC_CAPACITY_GROW'.
 */
inline static size_t increased_vec_cap(const size_t old_cap, const size_t target_cap) {
	if (target_cap <= old_cap)
		return old_cap;

	size_t new_cap = VEC_MIN_CAPACITY;
	while (new_cap < target_cap) {
		new_cap = VEC_CAPACITY_GROW(new_cap);
	}
	return new_cap;
}

vector_result vec_reserve(vector *const vec, const size_t additional) {
	if (vec->len + additional > vec->cap) {
		const size_t new_cap = increased_vec_cap(vec->cap, vec->cap + additional);

		/*
		 * new_cap != 0 because:
		 *  vec->cap >= 0 (size_t)
		 *  vec->len + additional > vec->cap
		 *  new_cap >= vec->len + additional
		 * therefore we can savely call reallocarray
		 */
		TYPE *const new_ptr = reallocarray(vec->ptr, new_cap, sizeof(TYPE));
		if (new_ptr == NULL) /* allocation failed */
			VEC_ALLOC_FAIL_REACTION("vec_reserve")

		vec->ptr = new_ptr;
		vec->cap = new_cap;
	}

	return VECTOR_SUCCESS;
}

vector_result vec_reserve_exact(vector *const vec, const size_t additional) {
	const size_t new_cap = vec->len + additional;

	if (new_cap > vec->cap) {
		/*
		 * new_cap != 0 because:
		 *  vec->cap >= 0 (size_t)
		 *  new_cap > vec->cap
		 * therefore we can savely call reallocarray
		 */
		TYPE *const new_ptr = reallocarray(vec->ptr, new_cap, sizeof(TYPE));
		if (new_ptr == NULL) /* allocation failed */
			VEC_ALLOC_FAIL_REACTION("vec_reserve_exact")

		vec->ptr = new_ptr;
		vec->cap = new_cap;
	}

	return VECTOR_SUCCESS;
}

bool vec_is_empty(const vector *const vec) {
	return vec->len == 0;
}

void vec_retain(vector *const vec, bool (*pred)(TYPE const *const)) {
	size_t curr = 0;
	size_t next;

	for (next = 0; next < vec->len; ++next) {
		if (pred(vec->ptr + next)) {
			if (curr != next) {
				memmove(vec->ptr + curr,
				        vec->ptr + next,
				        (vec->len - next) * sizeof(TYPE));
			}
			vec->len -= next - curr;
			next = curr;
			++curr;
		}
	}

	vec->len -= next - curr;
}

void vec_retain_unordered(vector *const vec, bool (*pred)(TYPE const *const)) {
	for (size_t i = 0; i < vec->len; ++i)
		if (!pred(vec->ptr + i)) {
			vec_swap_remove(vec, i, NULL);
			--i; /* repeat test since we swapped a different element in */
		}
}

void vec_foreach(const vector *const vec, void (*const func)(TYPE const *const)) {
	for (size_t i = 0; i < vec->len; ++i)
		(*func)(vec->ptr + i);
}

void vec_map(vector *const vec, void (*const func)(TYPE *const)) {
	for (size_t i = 0; i < vec->len; ++i)
		(*func)(vec->ptr + i);
}

vector_result vec_split_off(
		vector *const vec,
		vector *const into,
		const size_t at
	) {
	if (at >= vec->len)
		return VECTOR_IDX_FAIL;

	const size_t count = vec->len - at;

	const vector_result reserve_result = vec_reserve(into, count);
	if (reserve_result != VECTOR_SUCCESS)
		return reserve_result;

	memcpy(into->ptr + into->len, vec->ptr + at, count * sizeof(TYPE));
	into->len += count;
	vec->len  -= count;

	return VECTOR_SUCCESS;
}

vector_result vec_split_off_arr(
		vector *const vec,
		TYPE *const into,
		const size_t at
	) {
	if (at >= vec->len)
		return VECTOR_IDX_FAIL;

	const size_t count = vec->len - at;

	memcpy(into, vec->ptr + at, count * sizeof(TYPE));
	vec->len -= count;

	return VECTOR_SUCCESS;
}

vector_result vec_binary_search(
		const vector *const vec,
		TYPE const x,
		size_t *const idx,
		int (*const ord)(TYPE const *const, TYPE const *const)
	) {
	TYPE const *const ptr = vec->ptr;
	const size_t len = vec->len;

	size_t min = 0;
	size_t max = len - 1;
	size_t i = (min + max) / 2;

	while (min <= max) {
		const int order = (*ord)(ptr + i, &x);

		if (order == 0) { /* found it! */
			*idx = i;
			return VECTOR_SUCCESS;
		} else if (order > 0) { /* searched value is smaller */
			if (i == 0) { /* check to not underflow */
				*idx = 0;
				return VECTOR_IDX_FAIL;
			}
			max = i - 1;
		} else { /* searched value is greater */
			min = i + 1;
		}

		i = (min + max) / 2;
	}

	const int order = (*ord)(ptr + i, &x);
	*idx = order < 0 ? i + 1 : i;
	return VECTOR_IDX_FAIL;
}

bool vec_contains(const vector *const vec, TYPE const x) {
	for (size_t i = 0; i < vec->len; ++i)
		if (memcmp(vec->ptr + i, &x, sizeof(TYPE)) == 0)
			return true;

	return false;
}

bool vec_starts_with(const vector *const vec, TYPE const *const arr, const size_t len) {
	if (len > vec->len)
		return false;

	return memcmp(vec->ptr, arr, len * sizeof(TYPE)) == 0;
}

bool vec_ends_with(const vector *const vec, TYPE const *const arr, const size_t len) {
	if (len > vec->len)
		return false;

	return memcmp(
	        vec->ptr + vec->len - len,
	        arr,
	        len * sizeof(TYPE)) == 0;
}

bool vec_equals(const vector *const vec, const vector *const other) {
	return vec_equals_arr(vec, other->ptr, other->len);
}

bool vec_equals_arr(const vector *const vec, TYPE const *const arr, const size_t len) {
	if (vec->len != len)
		return false;

	return memcmp(vec->ptr, arr, len * sizeof(TYPE)) == 0;
}

int vec_cmp(
		const vector *const vec,
		const vector *const other,
		int (*const ord)(TYPE const *const, TYPE const *const)
	) {
	return vec_cmp_arr(vec, other->ptr, other->len, ord);
}

int vec_cmp_arr(
		const vector *const vec,
		TYPE const *const arr,
		const size_t len,
		int (*const ord)(TYPE const *const, TYPE const *const)
	) {
	TYPE *const ptr = vec->ptr;
	const size_t vec_len = vec->len;

	/*           min_len = min(vec_len, len) */
	const size_t min_len = vec_len > len ? len : vec_len;

	for (size_t i = 0; i < min_len; ++i) {
		const int order = (*ord)(ptr + i, arr + i);
		if (order != 0)
			return order;
	}

	/* we cannot do `return vec_len - len` here since they are size_t */
	if (vec_len > len)
		return 1;
	else if (vec_len == len)
		return 0;
	else
		return -1;
}

bool vec_is_sorted(const vector *const vec,
                   int (*const ord)(TYPE const *const, TYPE const *const)) {
	if (vec->len < 2)
		return true;

	for (size_t i = 0; i < vec->len - 1; ++i) {
		TYPE *const elem = vec->ptr + i;
		TYPE *const next = vec->ptr + i + 1;

		if ((*ord)(elem, next) > 0)
			return false;
	}

	return true;
}

void vec_reverse(vector *const vec) {
	for (size_t i = 0; i < vec->len / 2; ++i) {
		TYPE const temp = vec->ptr[vec->len - i - 1];
		vec->ptr[vec->len - i - 1] = vec->ptr[i];
		vec->ptr[i] = temp;
	}
}

void vec_rotate_right(vector *const vec, const size_t n) {
	if (vec->len == 0)
		return;

	const size_t rotation = n % vec->len;
	if (rotation == 0)
		return;

	size_t buffer_len = rotation > vec->len / 2 ? vec->len - rotation : rotation;
	const size_t buffer_size = buffer_len * sizeof(TYPE);

	if (buffer_size <= BUFFER_SIZE) {
		/*
		 * Using a buffer to copy the elements like this is about 10 times faster than the
		 * branch below where the vector gets reversed two times, but requires a buffer
		 * that has enough capacity to fit buffer_size bytes.
		 * It would be possible to just always put an array of buffer_size bytes on the
		 * stack but that might cause a crash when buffer_size gets too big.
		 */
		if (rotation > vec->len / 2) {
			memcpy(BUFFER, vec->ptr, buffer_size);
			memmove(vec->ptr,
					vec->ptr + buffer_len,
					(vec->len - buffer_len) * sizeof(TYPE));
			memcpy(vec->ptr + rotation, BUFFER, buffer_size);
		} else {
			memcpy(BUFFER, vec->ptr + (vec->len - buffer_len), buffer_size);
			memmove(vec->ptr + buffer_len,
					vec->ptr,
					(vec->len - buffer_len) * sizeof(TYPE));
			memcpy(vec->ptr, BUFFER, buffer_size);
		}
	} else {
		/* Since BUFFER is not big enough, try to create a buffer on the heap. */
		void *const buffer = malloc(buffer_size);

		if (buffer != NULL) {
			/* Same as above, just using buffer instead of BUFFER */
			if (rotation > vec->len / 2) {
				memcpy(buffer, vec->ptr, buffer_size);
				memmove(vec->ptr,
						vec->ptr + buffer_len,
						(vec->len - buffer_len) * sizeof(TYPE));
				memcpy(vec->ptr + rotation, buffer, buffer_size);
			} else {
				memcpy(buffer, vec->ptr + (vec->len - buffer_len), buffer_size);
				memmove(vec->ptr + buffer_len,
						vec->ptr,
						(vec->len - buffer_len) * sizeof(TYPE));
				memcpy(vec->ptr, buffer, buffer_size);
			}
			free(buffer);
		} else {
			/*
			 * BUFFER is not big enough and we can't allocate a new buffer on the heap.
			 * Use the simplest but slowest way of rotating the vector by first reversing
			 * the whole vector and then reverse the two parts seperated by the index
			 * `rotation`.
			 */

			vec_reverse(vec);

			/* These vectors MUST NOT be freed since they just point to `vec`s contents */
			vector first_part = {
				.ptr = vec->ptr,
				.len = rotation,
				.cap = vec->cap,
			};
			vector second_part = {
				.ptr = vec->ptr + rotation,
				.len = vec->len - rotation,
				.cap = vec->cap,
			};
			vec_reverse(&first_part);
			vec_reverse(&second_part);
		}
	}
}

void vec_rotate_left(vector *const vec, const size_t n) {
	if (vec->len == 0)
		return;

	const size_t rotation = n % vec->len;
	vec_rotate_right(vec, vec->len - rotation);
}

vector_result vec_find_next(
		const vector *const vec,
		TYPE const x,
		const size_t from,
		size_t *const idx
	) {
	for (size_t i = from; i < vec->len; ++i) {
		if (memcmp(vec->ptr + i, &x, sizeof(TYPE)) == 0) {
			*idx = i;
			return VECTOR_SUCCESS;
		}
	}

	return VECTOR_IDX_FAIL;
}

vector_result vec_find_next_arr(
		const vector *const vec,
		TYPE const *const arr,
		const size_t len,
		const size_t from,
		size_t *const idx
	) {
	for (size_t i = from; i <= vec->len - len; ++i) {
		if (memcmp(vec->ptr + i, arr, len * sizeof(TYPE)) == 0) {
			*idx = i;
			return VECTOR_SUCCESS;
		}
	}

	return VECTOR_IDX_FAIL;
}

vector_result vec_find_next_vec(
		const vector *const vec,
		const vector *const other,
		const size_t from,
		size_t *const idx
	) {
	return vec_find_next_arr(vec, other->ptr, other->len, from, idx);
}

vector_result vec_find_next_rev(
		const vector *const vec,
		TYPE const x,
		const size_t from,
		size_t *const idx
	) {
	if (from >= vec->len)
		return VECTOR_IDX_FAIL;

	for (size_t i = from + 1; i > 0; --i) {
		/*
		 * Since size_t is unsigned, we can't check whether i >= 0. Therefore `i` is
		 * the checked index + 1
		 */
		const size_t index = i - 1;
		if (memcmp(vec->ptr + index, &x, sizeof(TYPE)) == 0) {
			*idx = index;
			return VECTOR_SUCCESS;
		}
	}

	return VECTOR_IDX_FAIL;
}

vector_result vec_find_next_arr_rev(
		const vector *const vec,
		TYPE const *const arr,
		const size_t len,
		const size_t from,
		size_t *const idx
	) {
	if (from >= vec->len)
		return VECTOR_IDX_FAIL;

	for (size_t i = from - len + 2; i > 0; --i) {
		/*
		 * Since size_t is unsigned, we can't check whether i >= 0. Therefore `i` is
		 * the checked index + 1
		 */
		const size_t index = i - 1;
		if (memcmp(vec->ptr + index, arr, len * sizeof(TYPE)) == 0) {
			*idx = index;
			return VECTOR_SUCCESS;
		}
	}

	return VECTOR_IDX_FAIL;
}

vector_result vec_find_next_vec_rev(
		const vector *const vec,
		const vector *const other,
		const size_t from,
		size_t *const idx
	) {
	return vec_find_next_arr_rev(vec, other->ptr, other->len, from, idx);
}

TYPE *vec_ptr_to(const vector *const vec, const size_t idx) {
	if (idx >= vec->len)
		return NULL;

	return vec->ptr + idx;
}

vector_result vec_copy_to(
		const vector *const vec,
		const size_t start,
		const size_t len,
		TYPE *const into
	) {
	if (start >= vec->len || (len - start) > vec->len)
		return VECTOR_IDX_FAIL;

	memcpy(into, vec->ptr + start, len * sizeof(TYPE));
	return VECTOR_SUCCESS;
}

inline static void insertion_sort(
		TYPE *const arr,
		const size_t len,
		int (*const ord)(TYPE const *const, TYPE const *const)
	) {
	for (size_t i = 1; i < len; ++i) {
		for (size_t j = i; j > 0; --j) {
			const size_t idx = j - 1;
			const int order = (*ord)(arr + idx, arr + j);
			if (order <= 0)
				break;

			/* swap arr[idx] and arr[j] */
			TYPE const temp = arr[idx];
			arr[idx] = arr[j];
			arr[j] = temp;
		}
	}
}

/*
 * Merge two adjacent, already sorted slices together.
 * `buffer` is expected to fit at least `slice_len` elements
 *
 *  Example:
 *  slice_start    slice_split
 *  |              |
 *  v              v
 *  +----+----+----+----+----+----+----+
 *  |  4 | 15 | 40 |  1 | 19 | 47 | 49 |
 *  +----+----+----+----+----+----+----+
 *           ||
 *  merge_sorted(slice_start, slice_split, 7, ..);
 *           ||
 *           \/
 *  +----+----+----+----+----+----+----+
 *  |  1 |  4 | 15 | 19 | 40 | 47 | 49 |
 *  +----+----+----+----+----+----+----+
 */
inline static void merge_sorted(
		TYPE *const slice_start,
		TYPE *slice_split,
		const size_t slice_len,
		TYPE *const buffer,
		int (*const ord)(TYPE const *const, TYPE const *const)
	) {
	TYPE *slice_first = slice_start;
	TYPE *const slice_start_max = slice_split - 1;
	TYPE *const slice_split_max = slice_start + slice_len - 1;

	for (size_t i = 0; i < slice_len; ++i) {
		TYPE const first = *slice_first;
		TYPE const second = *slice_split;
		const int order = (*ord)(slice_first, slice_split);

		if (order <= 0) {
			*(buffer + i) = first;
			++slice_first;
		} else {
			*(buffer + i) = second;
			++slice_split;
		}

		/*
		 * check if one of the sub-slices is finished
		 * and copy the remainder of the other slice into buffer
		 */
		if (slice_first > slice_start_max) {
			memcpy(buffer + i + 1,
			       slice_split,
			       (size_t) (slice_split_max - slice_split + 1) * sizeof(TYPE));
			break;
		} else if (slice_split > slice_split_max) {
			memcpy(buffer + i + 1,
			       slice_first,
			       (size_t) (slice_start_max - slice_first + 1) * sizeof(TYPE));
			break;
		}
	}

	memcpy(slice_start, buffer, slice_len * sizeof(TYPE));
}

/* The size of a data slice we do want to use insertion sort on */
/* According to the (insufficient) testing I did, 16 seems to produce the fastest sort */
#define INSERTION_SORT_SIZE 16 / sizeof(TYPE)

void vec_sort(vector *const vec,
              int (*const ord)(TYPE const *const, TYPE const *const)
	) {
	TYPE *const ptr = vec->ptr;
	const size_t len = vec->len;

	if (INSERTION_SORT_SIZE >= len) {
		/* INSERTION_SORT_SIZE unfit, just use insertion sort on whole vector */
		insertion_sort(ptr, len, ord);
		return;
	}

	TYPE *const buffer = BUFFER_SIZE >= len * sizeof(TYPE)
		? BUFFER
		: calloc(len, sizeof(TYPE));

	if (buffer == NULL) {
		/* fallback to slower insertion sort since we don't have a buffer */
		insertion_sort(ptr, len, ord);
		return;
	}

	/*     slice_len = max(INSERTION_SORT_SIZE, 4); */
	size_t slice_len = INSERTION_SORT_SIZE < 4 ? 4 : INSERTION_SORT_SIZE;

	/* sort the slices */
	for (size_t i = 0; i <= len - slice_len; i += slice_len)
		insertion_sort(ptr + i, slice_len, ord);
	size_t remainder_len = len % slice_len;
	TYPE *remainder_ptr = ptr + len - remainder_len;
	insertion_sort(remainder_ptr, remainder_len, ord);

	/* merge the slices together */
	slice_len *= 2;
	for (; slice_len <= len; slice_len *= 2) {
		for (size_t i = 0; i <= len - slice_len; i += slice_len) {
			TYPE *const first_ptr = ptr + i;
			TYPE *const second_ptr = ptr + i + (slice_len / 2);
			merge_sorted(first_ptr, second_ptr, slice_len, buffer, ord);
		}
		/* ignore the last len % slice_len elements, we sort them later */
	}

	/* sort the remainder and merge it with the rest */
	remainder_len = len - slice_len / 2;
	remainder_ptr = ptr + len - remainder_len;

	insertion_sort(remainder_ptr, remainder_len, ord);
	merge_sorted(ptr, ptr + len - remainder_len, len, buffer, ord);

	/* free the buffer if it is an allocation */
	if ((void *) buffer != BUFFER)
		free(buffer);
}

bool vec_strip_prefix(vector *const vec, TYPE const *const prefix, const size_t len) {
	if (vec_starts_with(vec, prefix, len)) {
		memmove(vec->ptr, vec->ptr + len, (vec->len - len) * sizeof(TYPE));
		vec->len -= len;
		return true;
	} else
		return false;
}

bool vec_strip_suffix(vector *const vec, TYPE const *const suffix, const size_t len) {
	if (vec_ends_with(vec, suffix, len)) {
		vec->len -= len;
		return true;
	} else
		return false;
}

vector_result vec_swap(vector *const vec, const size_t a, const size_t b) {
	if (a >= vec->len || b >= vec->len)
		return VECTOR_IDX_FAIL;

	TYPE const temp = vec->ptr[a];
	vec->ptr[a] = vec->ptr[b];
	vec->ptr[b] = temp;

	return VECTOR_SUCCESS;
}

void vec_free(vector *const vec) {
	if (vec->ptr)
		free(vec->ptr);
}
